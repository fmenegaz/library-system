from geral.config import *

from modelo.livro import *

# inserindo a aplicação em um contexto :-/
with app.app_context():

    if os.path.exists(biblioteca):
        os.remove(biblioteca)

    # criar tabelas
    db.create_all()

    print("Banco de dados e tabelas criadas")