from geral.config import *

class Livro(db.Model):
    # atributos da pessoa
    id = db.Column(db.Integer, primary_key=True)
    idlivro = db.Column(db.Integer)
    titulo = db.Column(db.Text)
    autor = db.Column(db.Text)
    disponibilidade = db.Column(db.Text)
    localidade = db.Column(db.Text)

    # método para expressar a pessoa em forma de texto
    def __str__(self):
        return f"Titulo: {self.titulo}, Autor: {self.autor}, Disponibilidade: {self.disponibilidade}, \
        Localidade: {self.localidade}"

    # expressao da classe no formato json
    def json(self):
        return {
            "id": self.id,
            "idlivro": self.idlivro,
            "titulo": self.titulo,
            "autor": self.autor,
            "disponibilidade": self.disponibilidade,
            "localidade": self.localidade
        }