from geral.config import *
from modelo.livro import *


# exemplo de teste:
# curl localhost:5000/incluir/Pessoa -X POST -d '{"nome":"Teresa Có", "email":"teco@gmail.com","telefone":"47 9923-1232"}' -H 'Content-Type:application/json'

@app.route("/incluir/<string:classe>", methods=['post'])
def incluir(classe):
    # receber as informações do novo objeto
    dados = request.get_json()
    try:
        nova = None
        if classe == "Livro":
            nova = Livro(**dados)
        db.session.add(nova)  # adicionar no BD
        db.session.commit()

        return jsonify({"resultado": "ok", "detalhes": "ok"})
    except Exception as e:  # em caso de erro...
        # informar mensagem de erro :-(
        return jsonify({"resultado": "erro", "detalhes": str(e)})