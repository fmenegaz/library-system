// chamada ao backend
        var rota = 'http://191.52.7.71:5000/listar/Livro';

        // chamada ajax
        var acao = $.ajax({
            url: rota,
            //method: 'GET',
            dataType: 'json', // os dados são recebidos no formato json,
        });

        // se a chamada der certo
        acao.done(function (retorno) {
            // faz uma proteção contra erros
            try {
                if (retorno.resultado == "ok") {
                    // percorrer a lista de pessoas retornadas; 
                    for (var p of retorno.detalhes) { //p vai valer cada pessoa do vetor de pessoas
                        // https://stackoverflow.com/questions/8069663/avoiding-html-in-string-html-in-a-jquery-script
                        // criar um parágrafo
                        var paragrafo = $("<p>");
                        // informar o HTML deste parágrafo
                        // observe o apóstrofo inclinado, para interpretar as variáveis
                        paragrafo.html(`==> Titulo: ${p.titulo}, Autor: ${p.autor}, Disponibilidade: ${p.disponibilidade}, Localidade: ${p.localidade}`);
                        // adicionar o parágrafo criado na div
                        $('#listagem').append(paragrafo);
                    }
                } else {
                    alert("Erro informado pelo backend: " + retorno.detalhes);
                }
            } catch (error) { // se algo der errado...
                alert("Erro ao tentar fazer o ajax: " + error +
                    "\nResposta da ação: " + retorno.detalhes);
            }
        });

        // se a chamada der erro
        acao.fail(function (jqXHR, textStatus) {
            mensagem = encontrarErro(jqXHR, textStatus, rota);
            alert("Erro na chamada ajax: " + mensagem);
        });

        // função para encontrar o erro
        // https://stackoverflow.com/questions/6792878/jquery-ajax-error-function 
        function encontrarErro(jqXHR, textStatus, rota) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Não foi possível conectar, ' +
                    'verifique se o endereço do backend está certo' +
                    ' e se o backend está rodando.';
            } else if (jqXHR.status == 404) {
                msg = 'A URL informada não foi encontrada no ' +
                    'servidor [erro 404]: ' + rota;
            } else if (jqXHR.status == 500) {
                msg = 'Erro interno do servidor [erro 500], ' +
                    'verifique nos logs do servidor';
            } else if (textStatus === 'parsererror') {
                msg = 'Falha ao decodificar o resultado json';
            } else if (textStatus === 'timeout') {
                msg = 'Tempo excessivo de conexão, estourou o limite (timeout)';
            } else if (textStatus === 'abort') {
                msg = 'Requisição abortada (abort)';
            } else {
                msg = 'Erro desconhecido: ' + jqXHR.responseText;
            }
            return msg;
        }